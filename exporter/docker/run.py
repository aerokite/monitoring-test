from prometheus_client import start_http_server, Counter
import random
import time

cf = Counter('my_failures_total', 'Description of counter')

cs = Counter('my_success_total', 'Description of counter')

if __name__ == '__main__':
    # Start up the server to expose the metrics.
    start_http_server(8000)
    # Generate some requests.
    while True:
        time.sleep(5)
        cf.inc(random.randint(2,5))     # Increment
        cs.inc(random.randint(2,5))     # Increment
