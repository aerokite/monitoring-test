# Monitor Services

You can display your monitoring data exported by your services and collected by Prometheus.

First we need Prometheus

## Prometheus

We need to configure prometheus so that it can scrape correct metrics

```yaml
- source_labels: [__meta_kubernetes_service_annotation_prometheus_io_scrape]
  action: keep
  regex: food
```

Configuration in path `prometheus/config.yaml` will scrape all Kubernetes services if Service has annotation `prometheus.io/scrape: "food"`.

Now we need an exported.

## Exporter

Exporter container exposes metrics those we want to monitor.

Finally, display data in grafana.

## Grafana

In grafana, we need to add datasource "prometheus". And add panel with query
